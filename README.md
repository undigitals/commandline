### Basic command line

1. `ls` -> turgan joyimizdagi fayllar ro'yxati

2. `ls -la` -> turgan joyimizdagi fayllar ro'yxati (qo'shimcha data lar bilan birga)

3. `mkdir` -> papka yaratish

4. `rm -rf` -> papkani o'chirish

5. `touch ` -> fayl yaratish

6. `rm ` -> fayl o'chirish

7. `clear` -> tozalash

8. `cd` -> papkani ichiga kiradi

9. `cd ..` -> papkadan chiqish

10. `code .` -> vscode ni ochish terminaldan

11. `explorer .` -> terminaldan papkani ochish

---

### Basic npm

1. `npm init -y` ->
2. `npm i paketnomi` -> o'rnatish
3.
